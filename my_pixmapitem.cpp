#include "my_pixmapitem.h"

my_pixmapitem::my_pixmapitem(QGraphicsItem *parent) : QGraphicsPixmapItem(parent)
{
    index = 0;
}

int my_pixmapitem::getindex()
{
    return index;
}

void my_pixmapitem::setindex(int m_index)
{
    index = m_index;
}
