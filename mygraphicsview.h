#ifndef MYGRAPHICSVIEW_H
#define MYGRAPHICSVIEW_H

#include <QObject>
#include <QGraphicsView>
#include <qgraphicsview.h>
#include "my_pixmapitem.h"

class MyGraphicsView : public QGraphicsView
{
    Q_OBJECT
private:
    int scaleValue;
    bool flag;

protected:
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);

public:
    explicit MyGraphicsView(QWidget *parent = 0);


signals:
    void item(QGraphicsItem *item);
    void mouseMovePoint(QPointF viewPoint, QPointF scenePoint);

};

#endif // MYGRAPHICSVIEW_H
