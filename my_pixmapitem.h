#ifndef MY_PIXMAPITEM_H
#define MY_PIXMAPITEM_H
#include <QGraphicsPixmapItem>


class my_pixmapitem :public QGraphicsPixmapItem
{
public:
    explicit my_pixmapitem(QGraphicsItem *parent = 0);
    int index;

    int getindex();
    void setindex(int m_index);


};

#endif // MY_PIXMAPITEM_H
