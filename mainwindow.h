#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#define MAX_VERTEX_NUM 210

#include "findway.h"
#include <QMainWindow>
#include <QGraphicsScene>
#include "my_pixmapitem.h"
#include <QLabel>
#include <QtTextToSpeech>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT  //拓展了C++的功能，以便使用槽函数和信号

private:
    bool isclear;
    int step;
    FindWay *way;
    int endPointIndex;
    int startPointIndex;
    int waykind;
    QTextToSpeech *my_speech;
    QLabel *labItemCord;
    QLabel *labViewCord;
    QLabel *labSceneCord;
    QGraphicsScene *scene;
    QGraphicsPixmapItem *item;
    QGraphicsPixmapItem *startPixMapItem;
    QGraphicsPixmapItem *endPixMapItem;
    my_pixmapitem *position[100];



    typedef struct
    {
        int x;
        int y;

    }Point;
    Point pointList[MAX_VERTEX_NUM-1];

    void initQGraphics();
    bool drawPath(QVector<int> Path);


private slots:
    void findPath();
    void setStart(int startindex);
    void setEnd(int endindex);
    void reset();
    void setway();
    void chooseItem(QGraphicsItem *Pixmapitem);
    void speech(Path path);
    void on_mouseMovePoint(QPointF viewPoint, QPointF scenePoint);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
