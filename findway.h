#ifndef FINDWAY_H
#define FINDWAY_H
#define MAX_VERTEX_NUM 210
#define INF 9999

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <QObject>
#include <QVector>
#include <QDebug>

struct Path{
    QVector<int> path;
    QVector<int> distance;
};

class FindWay : public QObject
{
    Q_OBJECT
public:
    explicit FindWay(QObject *parent = nullptr);
    struct ArcCell{
        int adj;
        bool kind;
    };

    struct MGraph{
        ArcCell arcs[MAX_VERTEX_NUM][MAX_VERTEX_NUM];
        int vexnum;
    };

public:
    MGraph mgraph;
    int prev[MAX_VERTEX_NUM];
    int d[MAX_VERTEX_NUM];
    bool used[MAX_VERTEX_NUM];

    void CreateGraph();
    void speek(char *Text);
    Path findPath(int startPos,int endPos,int waykind);
};

#endif // FINDWAY_H
