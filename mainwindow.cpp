#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QPixmap>
#include <QGroupBox>
#include <QComboBox>
#include <QPushButton>

bool MainWindow::drawPath(QVector<int> path)
{

    QGraphicsPathItem *item = new QGraphicsPathItem();

    QPen pen;
    switch (waykind) {
    case 0:
        pen.setWidth (10);
        pen.setColor (Qt::red);
        break;
    case 1:
        pen.setWidth (10);
        pen.setColor (Qt::green);
        break;
    default:
        break;
    }
    item->setPen (pen);
    item->setFlag (QGraphicsItem::ItemIsPanel);


    QPainterPath pa;

    QPixmap startPixMap(":/images/images/startImages.png");
    QPixmap endPixMap(":/images/images/endImages.png");
    startPixMapItem = new QGraphicsPixmapItem(startPixMap);
    endPixMapItem = new QGraphicsPixmapItem(endPixMap);


    pa.moveTo (pointList[startPointIndex].x,pointList[startPointIndex].y);
    for (int i=1;i<path.size();i++)
    {
        pa.lineTo (pointList[path[i]-1].x,pointList[path[i]-1].y);
    }
    item->setPath (pa);
    scene->addItem (item);
    scene->addItem(startPixMapItem);
    scene->addItem(endPixMapItem);
    endPixMapItem->setOffset(pointList[path[path.size()-1]-1].x,pointList[path[path.size()-1]-1].y -33);
    startPixMapItem->setOffset(pointList[startPointIndex].x,pointList[startPointIndex].y -33);
    isclear = false;


    return true;
}

void MainWindow::initQGraphics()
{
    resize(1050,810);
    ui->graphicsView->setMinimumSize(1025, 810);
    QImage image(":/images/images/map.jpg");
    scene = new QGraphicsScene();
    item = new QGraphicsPixmapItem;
    item = scene->addPixmap(QPixmap::fromImage(image));
    ui->graphicsView->setScene(scene);
    ui->graphicsView->show();

}

void MainWindow::findPath()
{
    QPixmap startpixmap(":/images/images/start.png");
    QPixmap endpixmap(":/images/images/end.png");
    position[startPointIndex]->setPixmap(startpixmap);
    position[endPointIndex]->setPixmap(endpixmap);
    Path path;
    path = way->findPath(startPointIndex + 1,endPointIndex + 1,waykind);
    if(path.path == QVector<int>(0)) return;
    if(!isclear) reset();
    drawPath(path.path);
    step = 4;
    speech(path);

}

void MainWindow::setStart(int startindex)
{
    QPixmap startpixmap(":/images/images/start.png");
    position[startindex]->setPixmap(startpixmap);
    startPointIndex = startindex;
    qDebug()<<"startPointIndex:"<<startPointIndex;
}

void MainWindow::setEnd(int endindex)
{
    QPixmap endpixmap(":/images/images/end.png");
    position[endindex]->setPixmap(endpixmap);
    endPointIndex = endindex;
    qDebug()<<"endPointIndex:"<<endPointIndex;
}

void MainWindow::reset()
{

    QPixmap pospixmap(":/images/images/position.png");
    position[ui->endComboBox->currentIndex()]->setPixmap(pospixmap);
    position[ui->startComboBox->currentIndex()]->setPixmap(pospixmap);
    if(!isclear){
        scene->removeItem(scene->items()[0]);
        scene->removeItem(scene->items()[0]);
        scene->removeItem(scene->items()[0]);
        qDebug()<<scene->items()[0];
        my_speech->stop();
        ui->actBrowser->clear();
        isclear = true;
        step = 1;
    }
}

void MainWindow::setway()
{
    waykind = ui->wayComboBox->currentIndex();
}

void MainWindow::chooseItem(QGraphicsItem *Pixmapitem)
{
    QPixmap startpixmap(":/images/images/start.png");
    QPixmap endpixmap(":/images/images/end.png");
    QPixmap pospixmap(":/images/images/position.png");


    for(int i = 0;i<100;i++){
        if(Pixmapitem == position[i]){
            if(step ==  1) {
                ui->startComboBox->setCurrentIndex(position[i]->getindex());
                startPointIndex = position[i]->getindex();
                position[i]->setPixmap(startpixmap);
                step = 2;
            }
            else if (step == 2||step == 3) {
                position[ui->endComboBox->currentIndex()]->setPixmap(pospixmap);
                ui->endComboBox->setCurrentIndex(position[i]->getindex());
                endPointIndex = position[i]->getindex();
                position[i]->setPixmap(endpixmap);
                step = 3;
            }else if (step ==4)
            {
                reset();
            }
        }
    }
}

void MainWindow::speech(Path path)
{
    QString text ;
    QString sequence;
    int dist = 0;
    int i;
    int totaldist = 0;
    sequence.append("从"+ui->endComboBox->itemText(path.path[0]-1)+"前进");
    qDebug()<<sequence;
    dist = path.distance[0];

    for (i=1;i<path.path.size()-1;i++){

        if(ui->endComboBox->itemText(path.path[i]-1)==""){
            dist = dist + path.distance[i];
        }
        else{
        sequence.append(QString::number(dist)+"米,到达"+ui->endComboBox->itemText(path.path[i]-1)+"。");
        totaldist = totaldist + dist;
        dist = path.distance[i];
        text.append(sequence);
        ui->actBrowser->append(sequence);
        sequence.clear();
        sequence.append("再从"+ui->endComboBox->itemText(path.path[i]-1)+"前进");
        }
        qDebug()<<text;
        qDebug()<<ui->endComboBox->itemText(path.path[i]);

    }
    sequence.append(QString::number(dist)+"米,到达终点"+ui->endComboBox->itemText(path.path[i]-1)+"。");
    totaldist = totaldist + dist;
    if(ui->wayComboBox->currentIndex() == 0){
        text.append(sequence+"此行程共计"+QString::number(totaldist)+"m,预计花费时间"+QString::number(totaldist/80)+"分钟");
        ui->actBrowser->append(sequence+"\n"+"此行程共计"+QString::number(totaldist)+"m,预计花费时间"+QString::number(totaldist/80)+"分钟");
    }else {
        text.append(sequence+"此行程共计"+QString::number(totaldist)+"m,预计花费时间"+QString::number(totaldist/200)+"分钟");
        ui->actBrowser->append(sequence+"\n"+"此行程共计"+QString::number(totaldist)+"m,预计花费时间"+QString::number(totaldist/200)+"分钟");
    }
    my_speech->say(text);
    qDebug()<<text;
}

void MainWindow::on_mouseMovePoint(QPointF viewPoint, QPointF scenePoint)
{
    labViewCord->setText(QString::asprintf("view坐标:%.0f,%.0f", viewPoint.x(), viewPoint.y()));
    labSceneCord->setText(QString::asprintf("scene坐标:%.0f,%.0f", scenePoint.x(),scenePoint.y()));
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    endPointIndex = 0;
    startPointIndex = 0;
    waykind = 0;
    step = 1;
    labViewCord = new QLabel("view坐标:");
    labSceneCord = new QLabel("scene坐标:");
    ui->statusBar->addWidget(labViewCord);
    ui->statusBar->addWidget(labSceneCord);
    isclear = true;
    my_speech = new QTextToSpeech();
    my_speech->setLocale(QLocale::Chinese);//设置语言环境
    my_speech->setRate(0.0);//设置语速-1.0到1.0
    my_speech->setPitch(1.0);//设置音高-1.0到1.0
    my_speech->setVolume(1.0);//设置音量0.0-1.0

    way = new FindWay();
    int pointx[MAX_VERTEX_NUM-1]={ 228,  454,  598,  569,  977, 1169,  438,  405,  289,  284,
                                   285,  295,  301,  299,  597,  706,  758,  728,  451,  783,
                                   451,  751,  589,  570,  513,  690, 872, 1211, 1313, 1397,
                                  1366, 1130, 1125, 1131, 1418, 1265, 1496, 1532, 1526, 1211,
                                  1486, 1703, 1873, 2065, 2217, 1951, 1880, 1959, 2029, 1646,

                                  1593, 1630, 1703, 1778, 1850, 1651, 2257, 2425, 2387, 2350,
                                  2280, 2197, 2152, 2333, 2329, 2359, 2128, 2264, 2583, 2977,
                                  2974, 2978, 2997, 2977, 2976, 2747, 2897, 2757, 3182, 3039,
                                  1799, 2084, 2574, 2881, 2974, 3057, 3105,  458,  458,  458,
                                   316,  170,  389,  113,  174,  589,  664,  175, 1064, 1380,

                                  456,   634, 1160, 1265, 1122,  832,  615,  292,  905,  579,
                                  593,   589,  608,  603,  860,  845,  839, 1024, 1206, 1256,
                                  1419, 1013,  864,  910,  860, 1457,  852,  881,  750, 1388,
                                  1301,  1622, 1523, 1704, 1704, 1867, 1602, 1598, 1867, 1708,
                                  1876, 1880, 1974, 1979, 2211, 1703, 1865, 1799, 2066, 1757,

                                  1834, 1909, 2215, 2055, 2443, 1433, 1692, 1697, 1789, 1845,
                                  1740, 1888, 2051, 1658, 2186, 2306, 2041, 2043, 2139, 2110,
                                  2194, 2173, 2274, 2363, 2248, 2976, 2878, 2898, 2898, 2898,
                                  3026, 2755, 2896, 3090, 3221, 3119, 2966, 1609,  120,  311,
                                   598,  588,  464, 959,  1126, 402,  442,  442,  664,  306,

                                   392,  702,  682, 1158, 1478, 1366,  849, 2250, 845};

    int pointy[MAX_VERTEX_NUM-1]={ 337,  268,  215,  150,  229,  225,  639,  679,  802,  943,
                                   992, 1193, 1358, 1445, 1440, 1339, 1250, 1149, 1114, 1071,
                                  1011,  983,  884,  797,  712,  396,  567,  486,  668,  416,
                                   769,  786,  691, 1229, 1283, 1441, 1258, 1348, 1473, 1107,
                                   344,  494,  525,  698,  690,  999, 1126, 1271, 1376, 1605,

                                  1810, 1883, 2008, 2137, 2264, 2402, 2208, 2071, 2001, 1933,
                                  1802, 1678, 1591, 1365, 1429, 1514, 2705, 2614,  858,  246,
                                   467,  637,  956, 1193, 1305, 1973, 2039, 2108, 2044, 2462,
                                   865, 1480, 2041, 1557, 1562, 1916, 1812, 2690, 2615, 2477,
                                  2370, 2370, 2231, 1624, 1472, 1646, 1759, 2487, 2551, 2302,

                                   303,  273,  422,  586,  588,  377, 1014, 1118, 1146, 1122,
                                  1190, 1158, 1269, 1341, 1250, 1338, 1441, 1430,  914,  910,
                                   868, 1013,  710,  816,  850, 1374,  246,  369,  575, 1473,
                                  1050, 1400,  331,  335,  387,  225,  212,  235,  383,  678,
                                   680,  625,  624,  684,  373,  868,  877,  980,  883, 1267,

                                  1398, 1544,  875, 1168, 2366, 1568, 1710, 1887, 2029, 2153,
                                  2079, 2308, 2572, 2583, 2246, 2459, 1799, 1803, 1762, 1937,
                                  1898, 2070, 2014, 1690, 1754,  863, 1240, 1992, 2105, 2274,
                                  2301, 2037, 1814, 2178, 2323, 1898, 1808, 2150, 1473, 1490,
                                  1495, 1756, 2376, 1844, 2025, 1802, 1864, 1864, 1859, 1627,

                                  2371, 2304, 2370, 2398, 2587, 2156, 1029, 1754, 1495};
    pointList[MAX_VERTEX_NUM-1];
    for (int i = 0; i < MAX_VERTEX_NUM-1; ++i) {
        pointList[i].x = pointx[i];
        pointList[i].y = pointy[i];

    }

    connect(ui->resetButton, SIGNAL(clicked()), this, SLOT(reset()));
    connect(ui->endComboBox, SIGNAL(activated(int)), this, SLOT(setEnd(int)));
    connect(ui->wayComboBox, SIGNAL(activated(int)), this, SLOT(setway()));
    connect(ui->graphicsView,SIGNAL(item(QGraphicsItem*)),this, SLOT(chooseItem(QGraphicsItem*)));
    connect(ui->startComboBox, SIGNAL(activated(int)), this, SLOT(setStart(int)));
    connect(ui->findPathButton, SIGNAL(clicked()), this, SLOT(findPath()));

    QObject::connect(ui->graphicsView, SIGNAL(mouseMovePoint(QPointF, QPointF)),
                     this, SLOT(on_mouseMovePoint(QPointF, QPointF)));

    initQGraphics();
    QPixmap point(":/images/images/position.png");
    for (int i= 0;i<100;i++){
        position[i] = new my_pixmapitem();
        position[i]->setPixmap(point);
        position[i]->setindex(i);
        scene->addItem(position[i]);
        position[i]->setOffset(pointList[i].x-50,pointList[i].y-100);
        qDebug()<<"position"<<position[i];
    }
    ui->graphicsView->setCursor(Qt::CrossCursor);
    ui->graphicsView->setMouseTracking(true);

}

MainWindow::~MainWindow()
{
    delete ui;
}
