#include "mygraphicsview.h"
#include <QMouseEvent>
#include <QPoint>
#include <qmath.h>
#include <QDebug>

MyGraphicsView::MyGraphicsView(QWidget *parent) : QGraphicsView(parent)
{
    scaleValue = 0;
    flag = false;
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);//改变缩放中心为鼠标位置
    setResizeAnchor(QGraphicsView::AnchorUnderMouse);
    setDragMode(QGraphicsView::ScrollHandDrag);//可以长按移动
}


void MyGraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    QPoint viewPoint = event->pos();
    QPointF scenePoint = this->mapToScene(viewPoint);
    emit mouseMovePoint(viewPoint, scenePoint);
    QGraphicsView::mouseMoveEvent(event);

}

void MyGraphicsView::wheelEvent(QWheelEvent *event)
{
    int zoom = event->delta();
    if((zoom > 0) & (scaleValue<=4))
    {
        scale(1.1,1.1);
        scaleValue += 1;
    }
    else if((zoom < 0) & (scaleValue>=-12))
    {
        scale(1/1.1, 1/1.1);
        scaleValue -= 1;
    }
    //qDebug() <<"scaleValue:"<<scaleValue;

}

void MyGraphicsView::mouseDoubleClickEvent(QMouseEvent *event)
{
    QGraphicsView::mouseDoubleClickEvent(event);
    emit item(scene()->itemAt(mapToScene(event->pos()),transform()));


}
